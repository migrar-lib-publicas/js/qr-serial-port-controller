const devicesInstance = require('../models/devices').getInstance();
const devicesRepository = require("../repositories/devices.repository");

async function loadDevicesConfig() {
    try {
        const devices = await devicesRepository.getAll();
        for (const device of devices) {
            console.log(device);
            let actions = await device.getActions();
            if (device && actions && actions.length > 0) {
                let deviceInstance = devicesInstance.createDevice({
                    deviceType: device.type,
                    deviceBrand: device.brand,
                    deviceModel: device.model,
                    deviceVersion: device.version,
                    revokeInvitation: device.revoke_invitation,
                    networkMethod: actions[0].method,
                    networkIp: device.local_ip,
                    networkPort: actions[0].devices_gates_actions.channel_out
                });

                let channels = {};
                for (const action of actions) {
                    channels[action.devices_gates_actions.channel_out] = action.devices_gates_actions.channel_out;
                }

                let relayObject = {
                    type: device.type,
                    brand: device.brand,
                    model: device.model,
                    version: device.version
                };
                let networkObject = {
                    ip: device.local_ip,
                    port: actions[0].method.toUpperCase().trim() === "HTTP" ? 80 : null,
                    channel: channels,
                    url: actions[0].action.url || null,
                    headers: actions[0].action.headers || null,
                    body: actions[0].action.body || null,
                    method: actions[0].action.method || null
                };

                if (typeof deviceInstance.setRelay === "undefined") {
                    deviceInstance.setRelayAndNetwork(relayObject, networkObject);
                } else {
                    deviceInstance.setRelay(relayObject, networkObject);
                }
            } else {
                devicesInstance.createDevice({
                    deviceType: device.type,
                    deviceBrand: device.brand,
                    deviceModel: device.model,
                    deviceVersion: device.version,
                    revokeInvitation: device.revoke_invitation,
                    networkMethod: null,
                    networkIp: device.local_ip,
                    networkPort: null
                });
            }
        }
    } catch (error) {
        console.error('Error loading device configurations:', error);
    }
}

module.exports = { loadDevicesConfig };
