const Net = require('net');
const userController = require('../controllers/users.controller');

const lastInputQR = {};

// Global object to keep track of ruts
global.ipUserId = {};

function getMessageData(incomingString) {
    let re1 = /(?=PORTUNO).*(?=PORTUNO)/g;
    let re2 = /(?=NFCPORTUNO).*(?=NFCPORTUNO)/g;
    let matches1 = incomingString.match(re1);
    let matches2 = incomingString.match(re2);

    let match = '';

    if (matches1 && matches1.length > 0)
        match = matches1[0]

    if (matches2 && matches2.length > 0)
        match = matches2[0]

    if (match === '')
        match = incomingString

    return match;
}

function checkLastInput(lastInputQR, callerIp, msg) {
    const nowTimestamp = Math.floor(new Date().getTime());

    /** Si es la primera vez que se pasa el QR sobre el dispositivo o el msg anterior es
     * diferente del actual, se guarda el mensaje y pasa a procesarse */
    if(!lastInputQR[callerIp] || lastInputQR[callerIp].msg !== msg){
        lastInputQR[callerIp] = { msg: msg, timestamp: nowTimestamp };
        return true;
    }

    /** Diferencia entre mismo inputs, en milisegundos */
    const diffTime = nowTimestamp - lastInputQR[callerIp].timestamp;
    /** Diferencia límite permitida, en milisegundos */
    const min_diff_input_qr = Number(process.env.DIFF_INPUT_QR) || 2000;

    /** Si el msg es el mismo, pero la diferencia con el último es mayor a variable,
     * se guarda evento y pasa a procesarse */
    if (lastInputQR[callerIp].msg === msg && diffTime > min_diff_input_qr){
        lastInputQR[callerIp] = { msg: msg, timestamp: nowTimestamp };
        return true;
    }
    return false;
}

function processNotMx(chunk, callerIp, socket) {
    console.log('<< Process for non-MX QR readers >>');

    let data = chunk.toString();
    console.log(data);
    let match = getMessageData(data);

    console.log('TCP: Data recibida: ' + match);

    const isValid = checkLastInput(lastInputQR, callerIp, match);
    if(!isValid){
        console.log('Data recibida no es valida para procesarse');
        socket.write('code=0000&&desc=http');
        return;
    }

    if (data.includes('PORTUNO')) {
        console.log('PORTUNO: ',match, callerIp);
        /*
        let result = userController.processQr(match, callerIp);
        result.then((result) => {
            console.log(`Result is: ${result}`);
            if (result)
                socket.write('code=0000&&desc=http');
            else
                socket.write('code=0001&&desc=http');
        })
            .catch( error => {
                socket.write('code=0001&&desc=http');
            });
         */
    } else {
        socket.write('code=0001&&desc=http');
    }
}

function processMx(chunk, callerIp, socket) {
    console.log('<< Special process for MX QR readers >>');

    global.parsedData += chunk.toString();

    if (parsedData.endsWith('\n')){
        console.log('Data piece entitled for processing');
        let data = parsedData;
        global.parsedData='';
        let match = getMessageData(data);
        console.log('TCP: Received data: ' + match);
        const isValid = checkLastInput(lastInputQR, callerIp, match);
        if(!isValid){
            console.log('Unable to process data');
            socket.write('code=0000&&desc=http');
            return;
        }

        if (data.includes('PORTUNO')) {
            let result = userController.processQr(match, callerIp);

            result.then((result) => {
                console.log(`Result is: ${result}`);

                if (result)
                    socket.write('code=0000&&desc=http');
                else
                    socket.write('code=0001&&desc=http');
            })
                .catch( error => {
                    socket.write('code=0001&&desc=http');
                });

        } else {
            socket.write('code=0001&&desc=http');
            console.log('Invalid QR code!')
        }

    } else{
        console.log('Data not complete yet');
    }
}

// Intialize TCP server
function initTcpServer(tcpPort) {
    const server = new Net.Server();
    let parsedData = '';

    server.listen(tcpPort, function () {
        console.log(`TCP Server listening on port ${tcpPort}`);
    });

    server.on('connection', function (socket) {
        console.log(`A new connection has been established from: ${socket.remoteAddress}:${socket.remotePort}`);

        socket.on('data', function (chunk) {
            console.log(`Data received from client: ${chunk.toString()}`);
            let callerIp = socket.remoteAddress.replace(/^.*:/, '');
            const mxFlag = process.env.QR_MX_FLAG === 'true';

            if (mxFlag) {
                processMx(chunk, callerIp, socket, parsedData);
            } else {
                processNotMx(chunk, callerIp, socket);
            }
        });

        socket.on('end', function () {
            console.log('Closing connection with the client');
        });

        socket.on('error', function (err) {
            console.error(`Error: ${err}`);
        });
    });
}

module.exports = { initTcpServer };
