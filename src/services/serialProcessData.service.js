const { createReplyBuffer, handleQRCodeData, handleNFCData } = require('./dataHandlers');

/**
 * The function `processData` receives complete data and a port, validates the checksum, and based on the data type, either
 * handles QR code data or NFC data and sends a reply buffer to the port.
 * @param completeData - The `completeData` parameter is an array that contains the data received from a device. It is
 * assumed to be an array of bytes.
 * @param port - The `port` parameter is the communication port used to send and receive data. It is likely an instance of
 * a serial port object or a similar communication interface.
 */
async function processData(completeData, port) {

    console.log (' Validating checksum...');
    console.log('Complete Data Buffer: ',completeData);
    console.log('Complete Data: ',completeData.toString());

    let replyData={};

    //if (validateChecksum(completeData)) {
        if (completeData[3] === 0x20) {
            const deviceId = completeData[2];
            replyData = completeData[6] === 0x01 ? await handleQRCodeData(completeData, deviceId) : await handleNFCData(completeData, deviceId);
            return {...replyData, processable:true,  checksumValidation:true, notTimeRestricted: true, currentTime: Date.now(), replyCommandPermitted: true,};
        } else {
            console.log('Reply from command: ', completeData);
            return {processable:false,  checksumValidation:true, notTimeRestricted: true, currentTime: Date.now(), replyCommandPermitted: false,};
        }
    //} else {
        //console.error(`Checksum validation failed. Waiting for transmission retry...`);
        //return {processable:false, checksumValidation:false, notTimeRestricted: true, currentTime: Date.now(), replyCommandPermitted: false,};
    //}
}

/**
 * The function `replyCommand` sends a reply buffer to a specified port based on the validity of the device ID and the
 * provided configurations.
 * @param port - The `port` parameter is the communication port that is used to send the reply. It could be a serial port,
 * a network socket, or any other type of communication channel.
 * @param deviceId - The `deviceId` parameter is a unique identifier for the device that the reply is being sent to. It is
 * used to create the reply buffer and send the reply to the correct device.
 * @param isValid - isValid is a boolean value that indicates whether the received command is valid or not. If isValid is
 * true, it means the command is valid and the reply will include the accepted configurations. If isValid is false, it
 * means the command is invalid and the reply will include the rejected configurations.
 * @param configsAccept
 * @param configsReject
 */
function replyCommand(port, deviceId, isValid, [configsAccept, configsReject]){
    console.log('Sending Reply');
    const replyBuffer = createReplyBuffer(deviceId, isValid ? configsAccept : configsReject);
    port.write(replyBuffer, (err) => {
        if (err) console.error('Error sending reply:', err.message);
        else console.log('Reply sent successfully');
    });
}

/**
 * The function `validateChecksum` checks if the last byte of the given data array is equal to the calculated checksum of
 * the remaining bytes.
 * @param data - The `data` parameter is an array of bytes representing some data. The last byte in the array is the
 * checksum byte, which is used to validate the integrity of the data. The function calculates the checksum of the data by
 * summing up all the bytes in the array (excluding the checksum byte)
 * @returns a boolean value indicating whether the checksum byte in the given data matches the calculated checksum.
 */
function validateChecksum(data) {
    const checksumByte = data[data.length - 1];
    const dataWithoutChecksum = data.slice(0, -1);
    const calculatedChecksum = dataWithoutChecksum.reduce((acc, byte) => acc + byte, 0) & 0xFF;
    return checksumByte === calculatedChecksum;
}

module.exports = {processData, replyCommand}
