//const userController = require('../controllers/users.controller');

/**
 * The function `handleNFCData` takes in NFC data and a device ID, extracts the relevant NFC data, converts it to decimal,
 * logs the NFC card information, and then processes the NFC data using the `processQr` function from the `userController`
 * module.
 * @param data - The `data` parameter is the data received from an NFC device. It is expected to be a Buffer object
 * containing the raw data.
 * @param deviceId - The `deviceId` parameter is a unique identifier for the device that is sending the NFC data. It could
 * be a string or a number that uniquely identifies the device.
 * @returns the result of calling the `processQr` function from the `userController` object with the argument
 * `NFCPORTUNO${nfcDataDecimal}` and `deviceId`.
 */
async function handleNFCData(data, deviceId) {
    const size = (data[4] << 8) + data[5];
    const nfcData = data.slice(7, 6 + size);

    const nfcDataDecimal = parseInt(nfcData.toString('hex'), 16);

    const invertedData = parseInt( nfcDataDecimal.toString(16)
        .match(/.{1,2}/g)
        .reverse()
        .join(''), 16);

    return {method:'nfc', deviceId:deviceId, data:nfcDataDecimal, invertedData:invertedData, processable:true};
}

/**
 * The function `handleQRCodeData` takes in a data string and a device ID, extracts the QR code data from the string, logs
 * it to the console, and then processes the QR code data using the `processQr` function from the `userController` module.
 * @param data - The `data` parameter is the raw data received from scanning a QR code. It is expected to be a buffer or an
 * array of bytes.
 * @param deviceId - The `deviceId` parameter is a unique identifier for the device that scanned the QR code. It is used to
 * associate the QR code data with a specific device.
 * @returns the result of calling the `processQr` function from the `userController` module, passing in the `qrCodeData`
 * and `deviceId` as arguments.
 */
async function handleQRCodeData(data, deviceId) {
    const size = (data[4] << 8) + data[5];
    const qrCodeData = data.slice(7, 6 + size);
    return { method: 'qr', deviceId: deviceId, data: qrCodeData.toString(), processable: true };
}


/**
 * The `createReplyBuffer` function in JavaScript takes an ID and an array of configurations, and returns a buffer that
 * combines the ID, configurations, and checksum for a reply message.
 * @param id - The `id` parameter is a number that represents the ID of the reply buffer.
 * @param configs - The `configs` parameter is an array of objects. Each object represents a configuration and has the
 * following properties:
 * @returns The function `createReplyBuffer` returns a Buffer object that contains the combined data and checksum for the
 * given id and configs.
 */
function createReplyBuffer(id, configs) {
    const startBuffer = Buffer.from([
        0x01,
        0x33,
        id,
        0x21,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
        0x00,
    ]);

    const outputCount = Buffer.from([configs.length]);

    let configBuffers = [];
    for (let config of configs) {
        configBuffers.push(
            Buffer.from([
                config.hardwareNumber,
                config.actionMode,
                config.groups,
                config.actionsPerGroup,
                config.duration,
                config.interval,
                config.groupInterval,
            ]),
        );
    }

    let dataBuffer = Buffer.concat([
        startBuffer,
        outputCount,
        Buffer.concat(configBuffers),
    ]);

    let dataLength = dataBuffer.length - 6;
    dataBuffer[4] = (dataLength >> 8) & 0xff;
    dataBuffer[5] = dataLength & 0xff;

    let combinedBuffer = Buffer.concat([dataBuffer, Buffer.from([0x03])]);

    let checksum = 0;
    for (let i = 0; i < combinedBuffer.length; i++) {
        checksum += combinedBuffer[i];
    }
    checksum &= 0xff;

    const endBuffer = Buffer.from([checksum, 0x04]);

    return Buffer.concat([combinedBuffer, endBuffer]);
}

module.exports = {
    handleQRCodeData,
    handleNFCData,
    createReplyBuffer
};

