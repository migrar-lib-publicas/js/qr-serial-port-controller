const Net = require('net');
const { SerialPort } = require('serialport');
const { processData, replyCommand } = require('./serialProcessData.service');
const SerialPortOldService = require('./serialPortOld.service');

/* The `SerialPortService` class is a JavaScript class that handles serial port communication, including sending and
receiving data, and constructing reply signals. */
class SerialPortService {

    /**
     * The constructor function initializes the properties of an object with the provided arguments.
     * @param tcpPort
     * @param serialPortsArray
     * @param serialConfigStr - The `serialConfigStr` parameter is a string that represents the configuration settings for
     * the serial communication. It contains information such as baud rate, data bits, parity, and stop bits.
     * @param replyConfigStr - The `replyConfigStr` parameter is a string that contains the configuration for the reply
     * signals. It is used to construct the `replySignalConfig` object.
     * @param dt
     */
    constructor(tcpPort, serialPortsArray, serialConfigStr, replyConfigStr, dt) {
        this.tcpPort = tcpPort;
        this.portsArray = serialPortsArray;
        this.portConfig = this.convertSerialConfig(serialConfigStr);
        this.dataFragments = {};
        this.eotCharacter = {};
        this.replySignalConfig = this.constructReplySignal(replyConfigStr);
        this.dt = dt;
        this.tcpDataBuffer = "";
    }

    /**
     * The function sends a reply command with specified parameters and validity status.
     * @param isValid - A boolean value indicating whether the command is valid or not.
     * @param params - An object containing the following properties:
     */
    sendReplyCommand(isValid, params){
        replyCommand(params.port, params.deviceId, isValid, this.replySignalConfig);
    }

    /**
     * The function `constructReplySignal` takes a string as input and returns an array of configuration objects based on
     * the input.
     * @param replyConfigStr - The `replyConfigStr` parameter is a string that contains configuration information for
     * constructing a reply signal. It is expected to have the following format:
     * @returns The function `constructReplySignal` returns an array of arrays. Each inner array contains two objects. The
     * first object in each inner array is created using the values from the first four parts of the `replyConfigStr`
     * string, and the second object is created using the values from the last four parts of the `replyConfigStr` string.
     */
    constructReplySignal(replyConfigStr) {
        const colorMapping = { 'GREEN': 3, 'RED': 6 };
        const parts = replyConfigStr.split('-');

        const createConfigObject = (hardwareNumber, actionsPerGroup, duration, interval) => ({
            hardwareNumber,
            actionMode: 2,
            groups: 1,
            actionsPerGroup: parseInt(actionsPerGroup),
            duration: parseInt(duration),
            interval: parseInt(interval),
            groupInterval: 1
        });

        return [
            [
                createConfigObject(colorMapping[parts[0]], parts[1], parts[2], parts[3]),
                createConfigObject(0, parts[1], parts[2], parts[3])
            ],
            [
                createConfigObject(colorMapping[parts[4]], parts[5], parts[6], parts[7]),
                createConfigObject(0, parts[5], parts[6], parts[7])
            ]
        ];
    }

    /**
     * The function `convertSerialConfig` takes a string representing a serial configuration and returns an object with the
     * individual configuration values.
     * @param serialConfigStr - The `serialConfigStr` parameter is a string that represents the configuration of a serial
     * connection. It is expected to be in the format "baudRate-dataBits-parity-stopBits", where each component is
     * separated by a hyphen ("-").
     * @returns an object with the properties baudRate, dataBits, parity, and stopBits.
     */
    convertSerialConfig(serialConfigStr) {
        const [baudRate, dataBits, parity, stopBits] = serialConfigStr.split('-').map((val, index) =>
            index === 2 ? (val === 'N' ? 'none' : val.toLowerCase()) : parseInt(val, 10)
        );
        return { baudRate, dataBits, parity, stopBits };
    }

    /**
     * The function initializes ports by creating a new SerialPort object for each port in the portsArray and setting up
     * event listeners for data and error events.
     * @param callback - The `callback` parameter is a function that will be called after the ports have been initialized.
     * It is used to notify the caller that the initialization process has completed.
     */
    initializePorts(callback) {
        console.log('Initializing Ports!');

        this.portsArray.forEach(eachPort => {
            this.dataFragments[eachPort] = [];
            const port = new SerialPort({ path: '/dev/' + eachPort, ...this.portConfig });

            port.on('data', fragment => this.handleData(fragment, eachPort, port, callback));
            port.on('error', err => console.error('Error on port ' + eachPort + ': ', err.message));
        });

        const server = new Net.Server();

        server.listen(this.tcpPort, () => console.log(`TCP Server listening on port ${this.tcpPort}`));

        server.on('connection', (socket) => {
            console.log(`A new connection has been established from: ${socket.remoteAddress}:${socket.remotePort}`);

            socket.on('data', fragment => this.handleTcpData(fragment, socket, callback));

            socket.on('end', () => {
                console.log('Closing connection with the client');
            });

            socket.on('error', (err) => {
                console.error(`Error: ${err}`);
            });
        });
    }

    async handleTcpData(fragment, port, callback) {
        this.tcpDataBuffer += fragment.toString();

        let completeData;
        let dataType;

        if (this.tcpDataBuffer.startsWith('NFCPORTUNO')) {
            const endPattern = /(NFCPORTUNO[\n\r]?[\r\n]?)$/;
            if (endPattern.test(this.tcpDataBuffer)) {
                completeData = this.tcpDataBuffer.split('NFCPORTUNO')[1].replace(endPattern, '');
                dataType = 'nfc';
                this.tcpDataBuffer = "";
            }
        } else if (this.tcpDataBuffer.startsWith('PORTUNO')) {
            const endPattern = /(PORTUNO[\n\r]?[\r\n]?)$/;
            if (endPattern.test(this.tcpDataBuffer)) {
                completeData = this.tcpDataBuffer.split('PORTUNO')[1].replace(endPattern, '');
                dataType = 'qr';
                this.tcpDataBuffer = "";
            }
        } else {
            console.log('Data type unknown');
            this.tcpDataBuffer = "";
        }

        const result = await SerialPortOldService.processTcpData(completeData, port, this.dt);

        await callback({method: dataType, ...result}, port); //business logic will be executed here

    }

    /**
     * The function handles data fragments received from a port, checks for the end of transmission character, concatenates
     * the fragments, processes the complete data, and executes a callback function with the result.
     * @param fragment - The `fragment` parameter is a buffer that contains a portion of the data received from a serial
     * port.
     * @param eachPort - eachPort is a variable that represents the current port being processed. It is used to keep track
     * of the data fragments and eot characters specific to each port.
     * @param port - The `port` parameter is the port number or identifier for the data being handled. It is used to
     * identify which port the data is coming from or going to.
     * @param callback - The `callback` parameter is a function that will be called with the `result` and `port` arguments
     * once the data handling is complete. It is used to pass the result of the data processing to the caller or to perform
     * any additional business logic based on the processed data.
     */
    async handleData(fragment, eachPort, port, callback) {
        console.log('Handling data!');

        if (!this.dataFragments[eachPort]) {
            this.dataFragments[eachPort] = [];
        }

        if (this.dataFragments[eachPort].length === 0) {
            const startingBytes = fragment.slice(0, 2).toString('hex');
            if (startingBytes === '0133') {
                this.eotCharacter[eachPort] = 0x04;
            } else if (startingBytes === '5343') {
                this.eotCharacter[eachPort] = '#'.charCodeAt(0);
            } else {
                this.eotCharacter[eachPort] = 0x04;
            }
        }

        this.dataFragments[eachPort].push(fragment);

        const lastByteOfFragment = fragment[fragment.length - 1];
        if (lastByteOfFragment === this.eotCharacter[eachPort]) {
            const completeData = Buffer.concat(this.dataFragments[eachPort]);
            const result = await (this.eotCharacter[eachPort] === 0x04 ? processData(completeData, port) : SerialPortOldService.processData(completeData, this.dt));
            this.replyData = result;
            this.dataFragments[eachPort] = [];

            await callback(result, port);
        }
    }


}

module.exports = SerialPortService;
