const dataCache = new Map();
//const lastInputQR = {};
global.ipUserId = {};

/**
 * The function `processData` takes in data and a port name, splits the data into pieces, generates a hash, checks if the
 * data is valid, and then processes the data based on the port name.
 * processing.
 * @returns The function does not explicitly return anything.
 * @param inputBuffer
 * @param dt
 */
function processData(inputBuffer, dt) {
    const input = inputBuffer.toString();
    const methodPattern = /(PORTUNO|NFCPORTUNO)/;
    const deviceIdPattern = /^(\w+);/;
    const dataPattern = /(?:PORTUNO|NFCPORTUNO)(.*?)(?:PORTUNO|NFCPORTUNO)/;

    const methodMatch = input.match(methodPattern);
    const deviceIdMatch = input.match(deviceIdPattern);
    const dataMatch = input.match(dataPattern);

    const method = methodMatch ? (methodMatch[0] === 'PORTUNO' ? 'qr' : 'nfc') : null;
    const deviceId = deviceIdMatch ? deviceIdMatch[1] : null;
    const data = dataMatch ? dataMatch[1] : null
    const dataInt = parseInt(data);

    const currentTime = Date.now();
    const cacheKey = `${deviceId}-${data}`;
    const cacheEntry = dataCache.get(cacheKey);

    const invertedData = parseInt( dataInt.toString(16)
        .match(/.{1,2}/g)
        .reverse()
        .join(''), 16);

    console.log('invertedData : ',invertedData)

    if (cacheEntry && (currentTime - cacheEntry.timestamp < dt)) {
        return {
            method,
            deviceId,
            data,
            invertedData,
            processable: true,
            notTimeRestricted: false,
            replyCommandPermitted: false,
            currentTime
        };
    }

    dataCache.set(cacheKey, { timestamp: currentTime });

    return {
        method,
        deviceId,
        data,
        invertedData,
        processable: true,
        notTimeRestricted: true,
        replyCommandPermitted: false,
        currentTime
    };
}

async function processTcpData(completeData, port, dt) {

    const currentTime = Date.now();
    const cacheKey = `${port.remoteAddress}-${completeData}`;
    const cacheEntry = dataCache.get(cacheKey);

    if (cacheEntry && (currentTime - cacheEntry.timestamp < dt)) {
        return {
            deviceId: port.remoteAddress.replace(/^.*:/, ''),
            data: completeData,
            processable: true,
            notTimeRestricted: false,
            replyCommandPermitted: false,
            currentTime: currentTime
        };
    }

    dataCache.set(cacheKey, { timestamp: currentTime });

    return {
        deviceId: port.remoteAddress.replace(/^.*:/, ''),
        data: completeData,
        processable: true,
        notTimeRestricted: true,
        replyCommandPermitted: false,
        currentTime: currentTime
    };

}


module.exports = {  processData, processTcpData };
